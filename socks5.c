/*
 * TODO:
 * 1. cnvert memcpy to (int) to imporve performance
 * 2. 
 */
#include <stdio.h>
#include <string.h>
#include <malloc.h>
#include "socks5.h"

/* From RFC1928:
 * The client connects to the server, and sends a version
 * identifier/method selection message:
 *
 * +----+----------+----------+
 * |VER | NMETHODS | METHODS  |
 * +----+----------+----------+
 * | 1  |    1     | 1 to 255 |
 * +----+----------+----------+
 *
 * The values currently defined for METHOD are:
 * o  X'00' NO AUTHENTICATION REQUIRED (supported)
 * o  X'01' GSSAPI
 * o  X'02' USERNAME/PASSWORD (supported)
 * o  X'03' to X'7F' IANA ASSIGNED
 * o  X'80' to X'FE' RESERVED FOR PRIVATE METHODS
 * o  X'FF' NO ACCEPTABLE METHODS
 */
int parse_version_request(client_ctx_t *ctx)
{
	memcpy(&ctx->version, ctx->buffer.data, sizeof(socks5_version_request_t));
	ctx->status = VERSION_REQUEST;
	return 0;
}

/* From RFC1928:
 * The server selects from one of the methods given in METHODS, and
 * sends a METHOD selection message:
 *
 * +----+--------+
 * |VER | METHOD |
 * +----+--------+
 * | 1  |   1    |
 * +----+--------+
 */
int build_version_response(client_ctx_t *ctx)
{
	/* build a version response from client_config */
	socks5_version_response_t vrep;
	vrep.ver = SOCKS5_VERSION;
	vrep.method = NO_AUTHENICATION;

	ctx->status = VERSION_RESPONSE;
	memcpy(ctx->buffer.data, &vrep, sizeof(socks5_version_response_t));
	ctx->buffer.end = sizeof(socks5_version_response_t);
	return 0;
}

/* From RFC1929:
 * Once the SOCKS V5 server has started, and the client has selected the
 * Username/Password Authentication protocol, the Username/Password
 * subnegotiation begins.  This begins with the client producing a
 * Username/Password request:
 *
 * +----+------+----------+------+----------+
 * |VER | ULEN |  UNAME   | PLEN |  PASSWD  |
 * +----+------+----------+------+----------+
 * | 1  |  1   | 1 to 255 |  1   | 1 to 255 |
 * +----+------+----------+------+----------+
 *
 * The VER field contains the current version of the subnegotiation,
 * which is X'01'
 */
int parse_auth_request(client_ctx_t *ctx)
{
	/* TODO */
	ctx->status = AUTH_RESPONSE;
	return 0;
}

/* From RFC1929:
 * The server verifies the supplied UNAME and PASSWD, and sends the
 * following response:
 *
 * +----+--------+
 * |VER | STATUS |
 * +----+--------+
 * | 1  |   1    |
 * +----+--------+
 *
 * The VER field contains the current version of the subnegotiation,
 * which is X'01'
 *
 * A STATUS field of X'00' indicates success. If the server returns a
 *`failure' (STATUS value other than X'00') status, it MUST close the
 * connection.
 */
int build_auth_response(client_ctx_t *ctx)
{
	/* TODO */
	ctx->status = AUTH_REQUEST;
	return 0;
}

/* From RFC1928:
 * The SOCKS request is formed as follows:
 * +----+-----+-------+------+----------+----------+
 * |VER | CMD |  RSV  | ATYP | DST.ADDR | DST.PORT |
 * +----+-----+-------+------+----------+----------+
 * | 1  |  1  | X'00' |  1   | Variable |    2     |
 * +----+-----+-------+------+----------+----------+
 *
 * Where:
 * o  VER    protocol version: X'05'
 * o  CMD
 *	 o  CONNECT X'01' ( define in CMD_CONNECT )
 *	 o  BIND X'02'    ( define in CMD_BIND )
 *	 o  UDP ASSOCIATE X'03'  ( define in CMD_UDP )
 * o  RSV    RESERVED
 * o  ATYP   address type of following address
 *	 o  IP V4 address: X'01'
 *	 o  DOMAINNAME: X'03'
 *	 o  IP V6 address: X'04'
 * o  DST.ADDR       desired destination address
 * o  DST.PORT desired destination port in network octet
 *	 order
 *
 */
int parse_request(client_ctx_t *ctx)
{
	socks5_request_t req;

	ctx->status = REQUEST;
	memcpy(&req, ctx->buffer.data, sizeof(socks5_request_t));
	switch(req.atyp) {
		case IPV4:
			{
				ctx->atyp = IPV4;
				memcpy(&ctx->remote_ipv4.ipv4, 
						ctx->buffer.data + sizeof(socks5_request_t), 
						sizeof(ctx->remote_ipv4.ipv4));
				memcpy(&ctx->remote_ipv4.port, 
						ctx->buffer.data + sizeof(socks5_request_t) + sizeof(ctx->remote_ipv4.ipv4), 
						sizeof(ctx->remote_ipv4.port));
				break;
			}
		case DOMAIN: 
			{
				char *domain = malloc(sizeof(char) * 256);
				char domain_len;

				ctx->atyp = DOMAIN;
				/* get lenth */
				memcpy(&domain_len, 
						ctx->buffer.data + sizeof(socks5_request_t), 
						sizeof(domain_len));
				/* string */
				memcpy(domain, 
						ctx->buffer.data + sizeof(socks5_request_t) + sizeof(domain_len), 
						(uint8_t)domain_len);
				ctx->remote_domain.domain.str = domain;
				ctx->remote_domain.domain.size = (uint8_t)domain_len;
				/* port */
				memcpy(&ctx->remote_domain.port, 
						ctx->buffer.data + sizeof(socks5_request_t) + sizeof(domain_len) + (uint8_t)domain_len, 
						sizeof(ctx->remote_domain.port));
				/* domain -> ip  */
				domain_to_binary(&ctx->remote_domain, &ctx->remote_ipv4);
				break;
			}
		case IPV6:
			{
				ctx->atyp = IPV6;	
				/* TODO */
				break;
			}
		default:
			perror("request error");
	}

	switch(req.cmd) {
		case CONNECT: 
			{
				ctx->cmd = CONNECT;
				break;
			}
		case BIND: /* TCP/IP port binding */
			{
				ctx->cmd = BIND;
				break;
			}
		/* TODO: udp support */
		case UDP:
			{
				ctx->cmd = UDP;
				break;		
			}
		default:
			NULL;
	}
	
	return 0;
}

/* From RFC1928:
 * The server verifies the supplied UNAME and PASSWD, and sends the
 * following response:
 * +----+-----+-------+------+----------+----------+
 * |VER | REP |  RSV  | ATYP | BND.ADDR | BND.PORT |
 * +----+-----+-------+------+----------+----------+
 * | 1  |  1  | X'00' |  1   | Variable |    2     |
 * +----+-----+-------+------+----------+----------+
 *
 * Where:
 * o  VER    protocol version: X'05'
 * o  REP    Reply field:
 *	 o  X'00' succeeded
 *	 o  X'01' general SOCKS server failure
 *	 o  X'02' connection not allowed by ruleset
 *	 o  X'03' Network unreachable
 *	 o  X'04' Host unreachable
 *	 o  X'05' Connection refused
 * 	 o  X'06' TTL expired
 *	 o  X'07' Command not supported
 *	 o  X'08' Address type not supported
 *	 o  X'09' to X'FF' unassigned
 * o  RSV    RESERVED (must be set to 0x00)
 * o  ATYP   address type of following address
 * o  IP V4 address: X'01'
 *	 o  DOMAINNAME: X'03'
 *	 o  IP V6 address: X'04'
 * o  BND.ADDR       server bound address
 * o  BND.PORT       server bound port in network octet order
 *
 */
int build_response(client_ctx_t *ctx)
{
	socks5_response_t res;
	res.ver = SOCKS5_VERSION;
	res.rsv = RESERVED;

	ctx->atyp = IPV4; // only use ipv4
	ctx->status = RESPONSE;
	switch(ctx->atyp) {
		case IPV4:
			{
				res.atyp = IPV4;		
				/*
				memcpy(ctx->buffer.data + sizeof(socks5_response_t), 
						&ctx->remote_ipv4.ipv4, 
						sizeof(socks5_response_t));
				memcpy(ctx->buffer.data + sizeof(socks5_response_t) + sizeof(ctx->remote_ipv4.ipv4),
						&ctx->remote_ipv4.port,
						sizeof(ctx->remote_ipv4.port));
				ctx->buffer.end = sizeof(socks5_response_t) + sizeof(ctx->remote_ipv4.ipv4) + sizeof(ctx->remote_ipv4.port);
				*/
				memcpy(ctx->buffer.data + sizeof(socks5_response_t), 
						&ctx->local_ipv4.ipv4, 
						sizeof(socks5_response_t));
				memcpy(ctx->buffer.data + sizeof(socks5_response_t) + sizeof(ctx->local_ipv4.ipv4),
						&ctx->local_ipv4.port,
						sizeof(ctx->local_ipv4.port));
				ctx->buffer.end = sizeof(socks5_response_t) + sizeof(ctx->local_ipv4.ipv4) + sizeof(ctx->local_ipv4.port);
				break;
			}
		case DOMAIN:
			{
				uint8_t domain_len = ctx->remote_domain.domain.size;
				res.atyp = DOMAIN;	
				
				memcpy(ctx->buffer.data + sizeof(socks5_response_t),
						&domain_len,
						sizeof(domain_len));
				memcpy(ctx->buffer.data + sizeof(socks5_response_t) + sizeof(char),
						ctx->remote_domain.domain.str,
						ctx->remote_domain.domain.size);
				memcpy(ctx->buffer.data + sizeof(socks5_response_t) + sizeof(char) + ctx->remote_domain.domain.size,
						&ctx->remote_domain.port,
						sizeof(ctx->remote_domain.port));
				ctx->buffer.end = sizeof(socks5_response_t) + sizeof(domain_len) + domain_len + sizeof(ctx->remote_domain.port);
				break;
			}
		case IPV6:
			{
				res.atyp = IPV6;
				/* TODO */
				NULL;		
				break;
			}
		default:
			NULL;
	}

	switch(ctx->cmd) {
		case CONNECT:
			{
				res.rep = SUCCEEDED;
				ctx->connected = 1;
				break;
			}

		case BIND:
			{
				/* TODO */
				break;
			}
		case UDP:
			{
				/* TODO */
				break;	
			}

		default:
			NULL;
	}

	memcpy(ctx->buffer.data, &res, sizeof(socks5_response_t));
	return 0;
}
