#ifndef _CONFIG_H_
#define _CONFIG_H_

#include <stdint.h>

typedef struct {
	char *client_ip;
	uint16_t client_port;
	char *server_ip;
	uint16_t server_port;
} client_config_t;

typedef struct {
	char *client_ip;
	uint16_t server_port;
} server_config_t;

#endif /* _CONFIG_H */
