#include "net.h"

#include <sys/socket.h>
#include <netdb.h>
#include <string.h>
#include <arpa/inet.h>
#include <stdio.h>
#include <malloc.h>
#include <unistd.h>
#include <errno.h>

int set_socket_reuse(int socket_fd)
{
	int optval = 1;	
	int ret;
	ret = setsockopt(socket_fd, SOL_SOCKET, SO_REUSEADDR, &optval, sizeof(optval));
	return ret;
}

int create_socket()
{
	int fd;
	fd = socket(AF_INET, SOCK_STREAM, 0);
	return fd;
}

int bind_and_listen(int fd, ipv4_t *ipv4)
{
	struct sockaddr_in addr;
	memset(&addr, 0, sizeof(addr));
	addr.sin_addr.s_addr = ipv4->ipv4;
	addr.sin_port = ipv4->port;
	addr.sin_family = AF_INET;
	bind(fd, (struct sockaddr *)&addr, sizeof(addr));
	listen(fd, SOMAXCONN);
	return 0;
}

int connect_remote(client_ctx_t *ctx)
{
	int fd;
	struct sockaddr_in addr;
	memset(&addr, 0, sizeof(addr));
	addr.sin_addr.s_addr = ctx->remote_ipv4.ipv4;
	addr.sin_port = ctx->remote_ipv4.port;
	addr.sin_family = AF_INET;
	fd = socket(AF_INET, SOCK_STREAM, 0);
	connect(fd, (struct sockaddr *)&addr, sizeof(addr));
	ctx->remote_fd = fd;

	memset(&addr, 0, sizeof(addr));
	socklen_t addr_len = sizeof(addr);
	getsockname(fd, (struct sockaddr *)&addr, &addr_len);
	ctx->local_ipv4.ipv4 = addr.sin_addr.s_addr;
	ctx->local_ipv4.port = addr.sin_port;
	return fd;
}

int domain_to_binary(domain_t *domain, ipv4_t *ipv4)
{
	struct addrinfo hints, *servinfo;//, *p;
	memset(&hints, 0, sizeof(hints));
	hints.ai_family = AF_INET; 
	hints.ai_socktype = SOCK_STREAM;

	getaddrinfo(domain->domain.str, NULL, &hints, &servinfo);

	//for(p = servinfo; p != NULL; p = p->ai_next) {
	ipv4->ipv4 = ((struct sockaddr_in *)servinfo->ai_addr)->sin_addr.s_addr;
	ipv4->port = domain->port;
	//	inet_ntop(p->ai_family, &((struct sockaddr_in *)p->ai_addr)->sin_addr, ipv4->ipv4, sizeof(ipv4->ipv4));
//	}
	freeaddrinfo(servinfo); 
	return 1;
}

int readall(client_ctx_t *ctx)
{
	char *buf = ctx->buffer.data;
	ssize_t ret;
	ssize_t len = ctx->buffer.size;
	while(len != 0 && (ret = read(ctx->remote_fd, buf, len)) != 0) {
		if(ret == -1) {
			if(errno == EINTR)	
				continue;
			perror("read");
			break;
		}	
		printf("loop\n");
		len -= ret;
		buf += ret;
	}
	ctx->buffer.end = ctx->buffer.size - len;
	return ctx->buffer.end;
}

int writeall(client_ctx_t *ctx)
{
	ssize_t nleft;
	ssize_t nwritten;
	char *ptr;

	ptr = ctx->buffer.data;
	nleft = ctx->buffer.end;

	while(nleft > 0) {
		if((nwritten = read(ctx->remote_fd, ptr, nleft)) <= 0) {
			if(nwritten < 0 && errno == EINTR)	
				nwritten = 0;
			else return -1;
		}

		nleft -= nwritten;
		ptr += nwritten;	
	}
	return ctx->buffer.end;
}
