#include <stdio.h>
#include <unistd.h>
#include <netinet/in.h>
#include <netinet/tcp.h>
#include <arpa/inet.h>
#include <string.h>
#include <stdint.h>
#include <netdb.h>
#include <malloc.h>
#include <errno.h>

#include "socks5.h"
#include "client.h"
#include "net.h"

int init_client_ctx(client_ctx_t *ctx)
{
	memset(ctx, 0, sizeof(client_ctx_t));
	ctx->buffer.data = malloc(sizeof(char) * 4096);
	ctx->buffer.size = 4096;
	ctx->buffer.end = 0;

	ctx->remote_domain.domain.str = malloc(sizeof(char) * 64);
	ctx->remote_domain.domain.size = 64;

	//ctx->content.data = malloc(sizeof(char) * 4096);
	//ctx->content.size = 4096;
	//ctx->content.end = 0;
	return 1;
}

int main()
{
	struct sockaddr_in servaddr;
	struct sockaddr_in client_addr;
	int connfd, listenfd;
	//buf_t buffer;
	//buffer.size = 4096;
	//buffer.data = malloc(sizeof(char) * 4096);
	client_ctx_t *ctx = malloc(sizeof(client_ctx_t));
	init_client_ctx(ctx);

	memset(&servaddr, 0, sizeof(servaddr));

	servaddr.sin_family = AF_INET;
	servaddr.sin_port = htons(6666);
	servaddr.sin_addr.s_addr = htonl(INADDR_ANY);

	listenfd = socket(AF_INET, SOCK_STREAM, 0);

	int opt = 1;
	setsockopt(listenfd, SOL_SOCKET, SO_REUSEADDR, &opt, sizeof(opt));
	setsockopt(listenfd, IPPROTO_TCP, TCP_NODELAY, &opt, sizeof(opt));
	bind(listenfd, (struct sockaddr *)&servaddr, sizeof(servaddr));
	listen(listenfd, SOMAXCONN);
	while(1) {
		socklen_t client_size = sizeof(client_addr);
		connfd = accept(listenfd, (struct sockaddr *)&client_addr, &client_size);	
		ctx->socket_fd = connfd;

		/* 1. recv version require */
		int n = 0;
		n = recv(ctx->socket_fd, ctx->buffer.data, sizeof(socks5_version_request_t), 0);
		parse_version_request(ctx);
		/* 2. send version response */
		build_version_response(ctx);
		send(ctx->socket_fd, ctx->buffer.data,ctx->buffer.end, 0);
		/* 3. get request */
		n = recv(connfd, ctx->buffer.data, ctx->buffer.size, 0);
		ctx->buffer.end = n;
		parse_request(ctx);
		printf("\nRequest: %s\n", ctx->remote_domain.domain.str);
		printf("Request: recved %d bytes\n", n);

		/* 4. send response */
		connect_remote(ctx);

		char ip[16];
		inet_ntop(AF_INET, &ctx->remote_ipv4.ipv4, ip, 16);
		printf("Remote is %s\n", ip);
		inet_ntop(AF_INET, &ctx->local_ipv4.ipv4, ip, 16);
		printf("local is %s:%d\n", ip, ctx->local_ipv4.port);

		build_response(ctx);
		n = send(ctx->socket_fd, ctx->buffer.data, ctx->buffer.end, 0);
		printf("Response: sent %d bytes\n", n);

		/* 5. get data from local client */
		n = recv(ctx->socket_fd, ctx->buffer.data, ctx->buffer.size, 0);
		ctx->buffer.end = n;
		printf("Client: sent %d bytes\n", n);
//		printf("Get data from client: %s", ctx->buffer.data);

		/* 6. send to remote  */
		send(ctx->remote_fd, ctx->buffer.data, ctx->buffer.end, 0);

		n = recv(ctx->remote_fd, ctx->buffer.data, ctx->buffer.size, 0);
		ctx->buffer.end = n;
		printf("Remote: sent %d bytes\n", n);

//		n = recv(ctx->remote_fd, ctx->buffer.data + n, ctx->buffer.size, 0);
//		ctx->buffer.end += n;
//		printf("Remote: sent %d bytes\n", n);
//		printf("Get data from remote: %s\n", ctx->content.data);

		send(ctx->socket_fd, ctx->buffer.data, ctx->buffer.end, 0);

		//close(fd);
		//close(connfd);
	}
	close(listenfd);
	return 0;
}
