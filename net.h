#ifndef NET_H_
#define NET_H_

#include "client.h"

int set_socket_reuse(int socket_fd);
int create_socket();
int bind_and_listen(int fd, ipv4_t *ip);
int connect_remote(client_ctx_t *);
int domain_to_binary(domain_t *s, ipv4_t *ipv4);
int readall(client_ctx_t *);
int writeall(client_ctx_t *);

#endif
