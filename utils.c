/*
#include "utils.h"
#include "client.h"

#include <sys/socket.h>
#include <netdb.h>
#include <string.h>
#include <arpa/inet.h>
#include <stdio.h>
#include <malloc.h>
#include <unistd.h>
#include <errno.h>

int set_socket_reuse(int socket_fd)
{
	int optval = 1;	
	int ret;
	ret = setsockopt(socket_fd, SOL_SOCKET, SO_REUSEADDR, &optval, sizeof(optval));
	return ret;
}

int create_socket()
{
	int fd;
	fd = socket(AF_INET, SOCK_STREAM, 0);
	return fd;
}

int bind_and_listen(int fd, ipv4_t *ipv4)
{
	struct sockaddr_in addr;
	memset(&addr, 0, sizeof(addr));
	addr.sin_addr.s_addr = ipv4->ipv4;
	addr.sin_port = ipv4->port;
	addr.sin_family = AF_INET;
	bind(fd, (struct sockaddr *)&addr, sizeof(addr));
	listen(fd, SOMAXCONN);
	return 0;
}

int connect_remote(client_ctx_t *ctx)
{
	int fd;
	struct sockaddr_in addr;
	memset(&addr, 0, sizeof(addr));
	addr.sin_addr.s_addr = ctx->remote_ipv4.ipv4;
	addr.sin_port = ctx->remote_ipv4.port;
	addr.sin_family = AF_INET;
	fd = socket(AF_INET, SOCK_STREAM, 0);
	connect(fd, (struct sockaddr *)&addr, sizeof(addr));
	ctx->remote_fd = fd;
	return fd;
}

int domain_to_binary(domain_t *domain, ipv4_t *ipv4)
{
	struct addrinfo hints, *servinfo;//, *p;
	memset(&hints, 0, sizeof(hints));
	hints.ai_family = AF_INET; 
	hints.ai_socktype = SOCK_STREAM;

	getaddrinfo(domain->domain.str, NULL, &hints, &servinfo);

	//for(p = servinfo; p != NULL; p = p->ai_next) {
	ipv4->ipv4 = ((struct sockaddr_in *)servinfo->ai_addr)->sin_addr.s_addr;
	ipv4->port = domain->port;
	//	inet_ntop(p->ai_family, &((struct sockaddr_in *)p->ai_addr)->sin_addr, ipv4->ipv4, sizeof(ipv4->ipv4));
//	}
	freeaddrinfo(servinfo); 
	return 1;
}

int readall(client_ctx_t *ctx)
{
	ssize_t ret;
	ctx->content.end = ctx->content.size;
	while(ctx->content.end !=0 && (ret = read(ctx->remote_fd, ctx->content.data, ctx->content.end)) != 0) {
		if(ret == -1) {
			if (errno == EINTR)	
				continue;
			perror("read");
			break;
		}	
		ctx->content.end -= ret;
		ctx->content.data += ret;
	}
	return ctx->content.end;
}

int writeall(client_ctx_t *ctx)
{
	ssize_t ret;
	while(ctx->content.end !=0 && (ret = write(ctx->remote_fd, ctx->content.data, ctx->content.end)) != 0) {
		if(ret == -1) {
			if (errno == EINTR)	
				continue;
			perror("write");
			break;
		}	
		ctx->content.end -= ret;
		ctx->content.data += ret;
	}
	return ctx->content.end;
}
*/
