# client.c  client.h  config.h  log.h   socks5.c  socks5.h  utils.c  utils.h 
#objects = socks5.o utils.o client.o net.o
objects = socks5.o client.o net.o

socks5_client : $(objects)
		cc -o socks5_client $(objects)

client.o : client.c client.h socks5.h utils.h
		cc -c client.c

#utils.o : utils.c utils.h 
#		cc -c utils.c

net.o : net.c client.h utils.h
		cc -c net.c

socks5.o : socks5.c socks5.h utils.h client.h net.h
		cc -c socks5.c

clean :
		rm socks5_client $(objects)
