#ifndef _CLIENT_H_
#define _CLIENT_H_
#include <netinet/in.h>

#include "utils.h"
//#include "config.h"

#define VERSION_REQUEST 0x00
#define VERSION_RESPONSE 0x01
#define AUTH_REQUEST 0x02
#define AUTH_RESPONSE 0x03
#define REQUEST 0x04
#define RESPONSE 0x05

typedef struct {
	char version; /* version */
	char status; /* 6 status */
	//char auth; /* whether to auth */
	char cmd; /* connect bind udp */
	char atyp; /* ipv4 domain ipv6 */
	char rep; /* reply status, 0 stands succeeded */
	char connected; /* whether connected */
	//char listen; /* whether listened */
	int socket_fd; /* socket fd */
	int remote_fd; /* remote fd */
	ipv4_t remote_ipv4;
	domain_t remote_domain; /* domain */
	ipv4_t local_ipv4;
	buf_t buffer; /* lead to a buffer */
	//buf_t content;
	//client_config_t client_config;
	//server_config_t server_config;
} client_ctx_t;

int init_client_ctx(client_ctx_t *);
int del_client_ctx(client_ctx_t *);

#endif /* CLIENT_H_ */
