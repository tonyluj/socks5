#ifndef _SOCKS5_H_
#define _SOCKS5_H_

#include <stdint.h>
#include <netinet/in.h>

//#include "utils.h"
//#include "config.h"
#include "client.h"
#include "net.h"

#define SOCKS5_VERSION 0x05
#define RESERVED 0x00
#define NO_AUTHENICATION 0x00
#define CONNECT 0x01
#define BIND 0x02
#define UDP 0x03

#define IPV4 0x01
#define DOMAIN 0x03
#define IPV6 0x04
#define SUCCEEDED 0x00
#define FAILURE 0x01
#define CMD_NOT_SUPPORTED 0x07

/* Socks5 version packet */
#pragma pack(push, 1)
typedef struct {
	char ver;
	char nmethods;
	char methods[5];
} socks5_version_request_t;

/* Socks5 version packet response */
typedef struct {
	char ver;
	char method;
} socks5_version_response_t;

/* Socks5 authentication packet */
typedef struct {
	char ver;
	char ulen;
	char uname[256];
	char plen;
	char passwd[256];
} socks5_auth_request_t;

/* Socks5 authentication packet response */
typedef struct {
	char ver;
	char status;
} socks5_auth_response_t;

/* Socks5 request packet */
typedef struct {
	char ver;
	char cmd;
	char rsv;
	char atyp;
	/*char dstadr;
	unsigned short dstport;*/
} socks5_request_t;

/* Socks5 request packet response */
typedef struct {
	char ver;
	char rep;
	char rsv;
	char atyp;
	//struct in_addr addr; /* uint32_t */
	//uint16_t  bndport;
} socks5_response_t;
#pragma pack(pop)

int parse_version_request(client_ctx_t *);
int build_version_response(client_ctx_t *);
int parse_auth_request(client_ctx_t *);
int build_auth_response(client_ctx_t *);
int parse_request(client_ctx_t *);
int build_response(client_ctx_t *);

#endif /* _SOCKS5_H */
