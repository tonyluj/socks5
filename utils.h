#ifndef UTILS_H_
#define UTILS_H_

#include <netinet/in.h>

#define BUFFERSIZE 4096

typedef struct {
	char *data;
	ssize_t size;
	ssize_t start;
	ssize_t end;
} buf_t;

typedef struct {
	char *str;
	ssize_t size; /* not involved \0 */
} string_t;

typedef struct {
	string_t domain; /* domain */
	uint16_t port; /* port */
} domain_t;

typedef struct {
	uint32_t ipv4;
	uint16_t port;
} ipv4_t;


#endif /* UTILS_H */
